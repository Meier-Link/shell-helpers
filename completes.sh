#! /bin/bash

# A default script to be executed by the bashrc script in order to add custom
# completions
# Add `source $PATH_TO_THIS/complete.sh` to make it work
# Then create a $COMPLETE_DIR/bin/completes directory where to store completion
# scripts which are simple bash scripts (see completions.sh for an example).

COMPLETE_DIR=$HOME/bin/completes

if [ ! -d $COMPLETE_DIR ]
then
    mkdir -p $COMPLETE_DIR
fi

for cmp_file in $(ls $COMPLETE_DIR)
do
    if [ -f $COMPLETE_DIR/$cmp_file ]
    then
        source $COMPLETE_DIR/$cmp_file
    fi
done

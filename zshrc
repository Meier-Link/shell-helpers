# /etc/zsh/zshrc: system-wide .zshrc file for zsh(1).
#
# This file is sourced only for interactive shells. It
# should contain commands to set up aliases, functions,
# options, key bindings, etc.
#
# Global Order: zshenv, zprofile, zshrc, zlogin

READNULLCMD=${PAGER:-/usr/bin/pager}

# For user shell (not root)
if [ -e /usr/share/terminfo/x/xterm-256color ]; then
    export TERM='xterm-256color'
else
    export TERM='xterm-color'
fi

if [[ "$TERM" != emacs ]]; then
[[ -z "$terminfo[kdch1]" ]] || bindkey -M emacs "$terminfo[kdch1]" delete-char
[[ -z "$terminfo[khome]" ]] || bindkey -M emacs "$terminfo[khome]" beginning-of-line
[[ -z "$terminfo[kend]" ]] || bindkey -M emacs "$terminfo[kend]" end-of-line
[[ -z "$terminfo[kich1]" ]] || bindkey -M emacs "$terminfo[kich1]" overwrite-mode
[[ -z "$terminfo[kdch1]" ]] || bindkey -M vicmd "$terminfo[kdch1]" vi-delete-char
[[ -z "$terminfo[khome]" ]] || bindkey -M vicmd "$terminfo[khome]" vi-beginning-of-line
[[ -z "$terminfo[kend]" ]] || bindkey -M vicmd "$terminfo[kend]" vi-end-of-line
[[ -z "$terminfo[kich1]" ]] || bindkey -M vicmd "$terminfo[kich1]" overwrite-mode

[[ -z "$terminfo[cuu1]" ]] || bindkey -M viins "$terminfo[cuu1]" vi-up-line-or-history
[[ -z "$terminfo[cuf1]" ]] || bindkey -M viins "$terminfo[cuf1]" vi-forward-char
[[ -z "$terminfo[kcuu1]" ]] || bindkey -M viins "$terminfo[kcuu1]" vi-up-line-or-history
[[ -z "$terminfo[kcud1]" ]] || bindkey -M viins "$terminfo[kcud1]" vi-down-line-or-history
[[ -z "$terminfo[kcuf1]" ]] || bindkey -M viins "$terminfo[kcuf1]" vi-forward-char
[[ -z "$terminfo[kcub1]" ]] || bindkey -M viins "$terminfo[kcub1]" vi-backward-char

# ncurses fogyatekos
[[ "$terminfo[kcuu1]" == "O"* ]] && bindkey -M viins "${terminfo[kcuu1]/O/[}" vi-up-line-or-history
[[ "$terminfo[kcud1]" == "O"* ]] && bindkey -M viins "${terminfo[kcud1]/O/[}" vi-down-line-or-history
[[ "$terminfo[kcuf1]" == "O"* ]] && bindkey -M viins "${terminfo[kcuf1]/O/[}" vi-forward-char
[[ "$terminfo[kcub1]" == "O"* ]] && bindkey -M viins "${terminfo[kcub1]/O/[}" vi-backward-char
[[ "$terminfo[khome]" == "O"* ]] && bindkey -M viins "${terminfo[khome]/O/[}" beginning-of-line
[[ "$terminfo[kend]" == "O"* ]] && bindkey -M viins "${terminfo[kend]/O/[}" end-of-line
[[ "$terminfo[khome]" == "O"* ]] && bindkey -M emacs "${terminfo[khome]/O/[}" beginning-of-line
[[ "$terminfo[kend]" == "O"* ]] && bindkey -M emacs "${terminfo[kend]/O/[}" end-of-line
fi

zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
			     /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
#unalias run-help # XXX
#autoload run-help 

# If you don't want compinit called here, place the line
# skip_global_compinit=1
# in your $ZDOTDIR/.zshenv or $ZDOTDIR/.zprofice
if [[ -z "$skip_global_compinit" ]]; then
  autoload -U compinit
  compinit
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob nomatch notify
# End of lines configured by zsh-newuser-install

autoload -Uz compinit
compinit
# End of lines added by compinstall


# 0 : gris
# 1 : rouge
# 2 : vert
# 3 : jaune
# 4 : bleu
# 5 : mangenta
# 6 : cyan
# 7 : blanc

# repiquage d'un bout de la config sur zen

if [ "$TERM" = "xterm" -o "$TERM" = "xterm-color" -o "$TERM" = "xterm-debian" ]
then
  # affichage d'une partie du prompt dans le titre et l'icone
  precmd() {
    OLD_LANG=$LANG
    LANG=french
    TIME=$(date '+%A %d %B %Y')
    LANG=$OLD_LANG
    echo -en "\033]2;"$USER"@"$HOST" : "$TIME" ("$SHLVL")\007\033]1;"$HOST"\007"
  } 
  bindkey '^[OH' beginning-of-line 	  # Home
  bindkey '^[OF' end-of-line     	  # End
  bindkey '^[[5~' beginning-of-history	  # PageUp
  bindkey '^[[6~' end-of-history          # PageDown
  bindkey '\e[3~' delete-char             # Del
  bindkey '\e[2~' overwrite-mode          # Insert
fi

###########################################
# Options de zsh (cf 'man zshoptions')    #
###########################################

# Je ne veux JAMAIS de beeps
unsetopt beep
unsetopt hist_beep
unsetopt list_beep
# >| doit être utilisés pour pouvoir écraser un fichier déjà existant ;
# le fichier ne sera pas écrasé avec '>'
unsetopt clobber
# Ctrl+D est équivalent à 'logout'
unsetopt ignore_eof
# Affiche le code de sortie si différent de '0'
setopt print_exit_value
# Demande confirmation pour 'rm *'
unsetopt rm_star_silent
# Pas de mail warnings
unsetopt mail_warning

# Options de complétion
# Quand le dernier caractère d'une complétion est '/' et que l'on
# tape 'espace' après, le '/' est effacé
setopt auto_remove_slash
# Ne fait pas de complétion sur les fichiers et répertoires cachés
unsetopt glob_dots

# Traite les liens symboliques comme il faut
setopt chase_links

# Quand l'utilisateur commence sa commande par '!' pour faire de la
# complétion historique, il n'exécute pas la commande immédiatement
# mais il écrit la commande dans le prompt
setopt hist_verify
# Si la commande est invalide mais correspond au nom d'un sous-répertoire
# exécuter 'cd sous-répertoire'
setopt auto_cd
# L'exécution de "cd" met le répertoire d'où l'on vient sur la pile
setopt auto_pushd
# Ignore les doublons dans la pile
setopt pushd_ignore_dups
# N'affiche pas la pile après un "pushd" ou "popd"
setopt pushd_silent
# "pushd" sans argument = "pushd $HOME"
setopt pushd_to_home

# Les jobs qui tournent en tâche de fond sont nicé à '0'
unsetopt bg_nice
# N'envoie pas de "HUP" aux jobs qui tourent quand le shell se ferme
unsetopt hup

###############################################
# Paramètres de l'historique des commandes    #
###############################################

# Nombre d'entrées dans l'historique
export HISTORY=1000
export SAVEHIST=1000

# Fichier où est stocké l'historique
export HISTFILE=$HOME/.history

# Ajoute l'historique à la fin de l'ancien fichier
#setopt append_history

# Chaque ligne est ajoutée dans l'historique à mesure qu'elle est tapée
setopt inc_append_history

# Ne stocke pas  une ligne dans l'historique si elle  est identique à la
# précédente
setopt hist_ignore_dups

# Supprime les  répétitions dans le fichier  d'historique, ne conservant
# que la dernière occurrence ajoutée
#setopt hist_ignore_all_dups

# Supprime les  répétitions dans l'historique lorsqu'il  est plein, mais
# pas avant
setopt hist_expire_dups_first

# N'enregistre  pas plus d'une fois  une même ligne, quelles  que soient
# les options fixées pour la session courante
#setopt hist_save_no_dups

# La recherche dans  l'historique avec l'éditeur de commandes  de zsh ne
# montre  pas  une même  ligne  plus  d'une fois,  même  si  elle a  été
# enregistrée
setopt hist_find_no_dups

export SVN_EDITOR=vim

###############################################
# Utilisation de iBus pour le Japonais        #
###############################################

export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

###############################################
# Divers alias                                #
###############################################

#-- subversion
# commit
alias sucom='svn commit '
# import
alias suimp='svn import '
# checkout
alias sucheck='svn checkout '
# update
alias supdate='svn update '
# add
alias suadd='svn add '
# delete
alias sudel='svn delete '
# logs
alias sulog='svn log '
# diff
alias sudif='svn diff --revision '
#revert
alias surev='svn revert '

# ls coloré
#alias ls='ls --classify --tabsize=0 --literal --color=auto --show-control-chars --human-readable'
#alias ls='ls --tabsize=0 --literal --color=auto --show-control-chars --human-readable' # XXX
alias ll='ls -l'
alias la='ls -a'
# grep avec des couleurs
export GREP_COLOR=31
alias grep='grep --color=auto'
# recherche seulement dans les dossiers
alias dfind='find -type d -name ' # utilisation : dfind "*keywords*"
# recherche seulement les fichiers
alias ffind='find ! -type d -name ' # utilisation : ffind "*keywords*"

# Gestion de la couleur pour 'ls' (exportation de LS_COLORS)
if [ -x /usr/bin/dircolors ]
then
  if [ -r ~/.dir_colors ]
  then
    eval "`dircolors ~/.dir_colors`"
  elif [ -r /etc/dir_colors ]
  then
    eval "`dircolors /etc/dir_colors`"
  else
    eval "`dircolors`"
  fi
fi

# Demande confirmation avant d'écraser un fichier
alias cp='cp --interactive'
alias mv='mv --interactive'
alias rm='rm --interactive'

# tree more verbose
alias fulltree='tree -phug'
alias pathtree='tree -fi'

# scriptable tree
alias softree='tree -fi'

alias android-connect="mtpfs -o allow_other /media/Galaxy"
alias android-disconnect="fusermount -u /media/Galaxy"

# Compiling 32 bits with gcc
alias gcc32='gcc -m32'
alias gcc32db='gcc -m32 -fno-stack-protector -z execstack -g'

# quickly up to su
alias s='sudo -s'

# Historiques de modifications
# dernière heure
#alias modh="find /home/$USER/ -not -path '/home/$USER/.thumbnails*' -not -path '/home/$USER/.local*' -mmin -60"
# dernier jour
#alias modd="find /home/$USER/ -not -path '/home/$USER/.thumbnails*' -not -path '/home/$USER/.local*' -mtime -1"


###############################################
# Divers Others                               #
###############################################

export DERBY_HOME=/home/meier/glassfish-3.1.2/javadb

###############################################
# Prompt couleur                              #
###############################################

function git_branch() {
    br="$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/<(\1)>/') "
    if [ $? -eq 0 ]
    then
        echo -e "\e[33;1m$br\e[0m"
    fi
}

precmd() {
    eval "$PROMPT_COMMAND"
}
export PROMPT_COMMAND='git_branch'

# (la couleur n'est pas la même pour le root et pour les simples utilisateurs)
if [ "`id -u`" -eq 0 ]; then
  export PS1="%{[36;1m%}%T %{[34m%}%n%{[33m%}@%{[37m%}%m %j job(s) %{[32m%}%~%{[33m%}%#%{[0m%}
---> "
else
  export PS1="%{[36;1m%}%T %{[31m%}%n%{[33m%}@%{[37m%}%m %j job(s) %{[32m%}%~%{[33m%}%#%{[0m%}
---> "
fi

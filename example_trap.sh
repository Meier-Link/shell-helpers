#!/bin/bash
#
# Usage example for trap usage
# Inspired by http://www.linuxjournal.com/content/use-bash-trap-statement-cleanup-temporary-files

declare -a on_exit_items

function on_exit()
{
    # Run actions bound to be ran on EXIT.
    for i in "${on_exit_items[@]}"
    do
        echo "on_exit: $i"
        eval $i
    done
}

function add_on_exit()
{
    # Add action to be ran on EXIT
    local n=${#on_exit_items[*]}
    on_exit_items[$n]="$*"
    if [[ $n -eq 0 ]]; then
        echo "Setting trap"
        trap on_exit EXIT
    fi
}

touch foo.tmp  # Do something
add_on_exit rm -f foo.tmp
# Do other things

touch bar.tmp  # again ...
add_on_exit rm -f bar.tmp

ls -la

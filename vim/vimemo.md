Memo des commandes sympas de Vim <3
===================================

Augmenter/réduire l'indentation
-------------------------------

En mode visuel :
- n>> pour ajouter 'n' tabs
- n<< pour retirer 'n' tabs
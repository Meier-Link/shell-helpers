Meetup Vim #10
--------------

### Neo vim

Fork de vim qui a débuté il y a environ 1 an. Vim est un outil complexe, avec un
support d'un (très) grand nombre de machine.

L'idée est d'avoir une base de code beaucoup plus légère que "le" vim originel.

Le projet Neo Vim ralentit un peu, et ils ont maintenant besoin d'un lead dev
salarié.

Quelques pull requests importants de bloqué (langage de script Lua, compilation
sous Windows, ...)

Installation :
- Neo Vim est dispo dans la plupart des packages (Linux, Mac)
- Il existe un module Python pour les plugins en Python.
- Sous Linux, installer xsel pour la gestion du clipboard.
- Juste un lien symbolique vers la conf de vim pour la compabilité

Quelques soucis avec les params complexes, sinon la conf viom passe sur neo vim.

Beaucoup de featurs de Vim qui deviennent obsolètes.

- Quelques nouvelles featurs (ex: tnoremap) : Params par défaut (autoindent,
  autoread, mouse sur 'a' par défaut, etc.)
- Quelques subtilités
- Ajouts, comme msg-rpc (session remote), exécution asynchrone des plugins, ...
- Emulation de terminal (cf. tnoremap), customisable avec :e term //<executable>

### Dot files versionning

Les fichiers .ququchose (ex: .vimrc).

- Personnalisation de la machine.
- Customization itérative : amélioration progressive de son outil perso.
- Nécessite le versionning (pour faciliter la récupération et les tests entre
  les fonctionnalités).
- Partager sa config.

#### Pour optimiser la récupération des dotfiles versionnés : 

- rcm : github.com/thoughtbog/rcm
- homeshick : github.com/andsen/homeshick
- myrepos : myrepos.branchable.com

Voir aussi dotfiles.github.io : Une vingtaine d'outils du même genre.

Voir aussi :
- github.com/boxen (cf. puppet) pour avoir sa machine de dev prête à
  l'emploi. Attention : uniquement sur Mac.
- github.com/spencergibb/battleschool (ansible)

On peut aussi tracker les install de paquet.

Sur Mac, voir "brew cask" pour faciliter les install.

Amélioration à creuser : script type 'setup' qu'on peut exécuter n'importe quand
et qui s'occupera de mettre à jour les installs/config.

### Misc

Plutôt que de créer un thème de 0 (ou même copier un thème), chercher les 'hl
groups' (comme Search par exemple).

autocmd InsertEnter/InsertLeave * :colorscheme (à creuser pour modifier les couleurs en fx du
mode).

Sous Vim, il est possible de modifer les commandes déjà exécutées en modifiant
le register (à creuser, permet de manipuler les macros, par exemple).
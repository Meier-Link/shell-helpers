#! /usr/bin/env python

import os

DOC_DIR = 'Documents'

EXEC_FILETYPE = {
    'docx': '/usr/bin/libreoffice',
    'doc': '/usr/bin/libreoffice',
    'odt': '/usr/bin/libreoffice',
    'xsl': '/usr/bin/libreoffice',
    'xslx': '/usr/bin/libreoffice',
    'pdf': '/usr/bin/evince',
}

doc_path = os.path.join('/home', os.environ.get('USER'), DOC_DIR)


def get_exe(filepath):
    global EXEC_FILETYPE
    ext = os.path.splitext(filepath)[-1][1:]
    if ext in EXEC_FILETYPE.keys():
        return EXEC_FILETYPE[ext]
    return None

def dir_walk(dirpath):
    elems = os.listdir(dirpath)
    dirs = []
    files = []
    for e in elems:
        e_full = os.path.join(dirpath, e)
        if os.path.isdir(e_full):
            dirs.append(e)
        if os.path.isfile(e_full):
            files.append(e)
    return dirs, files

def get_files(dirpath):
    dirname = os.path.split(dirpath)[-1]
    rslt_files = {}
    elems = os.listdir(dirpath)

    #for dirpath, dirs, files in os.walk(dirpath):
    dirs, files = dir_walk(dirpath)

    for e in files:
        full_e = os.path.join(dirpath, e)
        exec_str = get_exe(e)
        if exec_str is not None:
            rslt_files[e] = '''{} "{}"'''.format(exec_str, full_e)

    for e in dirs:
        rslt_files[e] = get_files(os.path.join(dirpath, e))

    return rslt_files

def build_menu(dirname, tree):
    if dirname is not None:
        print """<menu id="docs-menu-{name}" label="{name}">""".format(
            name=dirname)
    for fname, assoc in tree.items():
        if type(assoc) is dict:
            build_menu(fname, assoc)
        else:
            print """<item label="{name}">
        <action name="Execute"><execute>{exe_path}</execute></action>
        </item>""".format(
                name=fname,
                exe_path=assoc
            )
    if dirname is not None:
        print '</menu>'

def build_pipe_menu(dirpath):
    tree = get_files(dirpath)
    print '<openbox_pipe_menu>'
    build_menu(None, tree)
    print '</openbox_pipe_menu>'

if __name__ == '__main__':
    build_pipe_menu(doc_path)

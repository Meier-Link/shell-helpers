#!/usr/bin/bash

# Script to run as root when creating a centOS VM from a raw image.

SIMPLE_USER=centos

# Update package list.
yum update -y

# Core utils
yum install -y epel-release coreutils net-tools wget tree
yum install -y yum-utils device-mapper-persistent-data lvm2
yum install -y zsh
yum install -y vim
yum install -y git

# Get my little customisations
cd /root  # Make sure we are in the root home
git clone https://framagit.org/Meier-Link/shell-helpers.git

(
    cd shell-helpers
    cp zshrc /etc/zshrc

    # Vim related
    # Start by pathogen
    mkdir -p /home/${SIMPLE_USER}/.vim/autoload /home/${SIMPLE_USER}/.vim/bundle && \
    curl -LSso /home/${SIMPLE_USER}/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    # Then add NERDTree
    git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
    # And finally add our custom touch
    cp vim/vimrc /home/${SIMPLE_USER}/.vimrc
    chown ${SIMPLE_USER}:${SIMPLE_USER} /home/${SIMPLE_USER}/.vimrc
    cd ..
    rm -rf shell-helpers
)

# Use zsh for everyone (need logout/login to be effective)
usermod --shell /bin/zsh root
usermod --shell /bin/zsh ${SIMPLE_USER}

# Docker CE
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
usermod -aG docker ${SIMPLE_USER}
systemctl start docker
systemctl enable docker

#! /bin/zsh

for c in {0..255}
do
    s="38;5"
    tag="\033[${s};${c}m"
    str="${s};${c}"
    echo -ne "${tag}test ${str}${NONE}"
echo
done

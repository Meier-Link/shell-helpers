#! /bin/bash

_complete_ticket () {
  cur=${COMP_WORDS[COMP_CWORD]}
  case "${cur}" in
    st) use="start" ;;
    co*) use="commit" ;;
    pu*) use="push" ;;
    li*) use="list" ;;
    cu*) use="current" ;;
    cl*) use="close" ;;
    me*) use="merge" ;;
    lo*) use="log" ;;
    h*)  use="help" ;;
  esac
  COMPREPLY=( $( compgen -W "$use" -- $cur ) )
}
complete -o default -o nospace -F _complete_ticket ticket

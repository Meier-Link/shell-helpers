#! /bin/bash

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -lF'
alias la='ls -a'
alias lla='ll -a'
alias l='ls -CF'

# grep with colors
export GREP_COLOR=31
alias grep='grep --color=auto'

# Search shortcuts ...
# ... only dirs
alias dfind='find -type d -name ' # utilisation : dfind '*keywords*'
# ... only files
alias ffind='find ! -type d -name ' # utilisation : ffind '*keywords*'

# Always ask before these actions ...
alias cp='cp --interactive'
alias mv='mv --interactive'
alias rm='rm --interactive'

# Wants 'tree' (very nice cli tool <3)
alias tree='tree --dirsfirst'
alias fulltree='tree -phug'
alias pathtree='tree -fi'
alias sizetree='tree --du'
# scriptable tree
alias softree='tree -fi'

# quickly up to su
alias s='sudo -s'

alias greplines='grep -rni'

# Count line of code
alias loc="cat **/*.py | sed '/^\s*#/d;/^\s*$/d' | wc -l"

# pythonics
alias sflake8='flake8 --max-line-length=89 --ignore=E128,F403'
alias py2='python'
alias ipy='ipython'
alias py3='python3'
alias py='py2'

# git shortcuts
alias ga='git add'
alias gc='git commit -m'
alias gl="git log --graph --pretty=format:'%Cred%h%Creset - %C(yellow)%d %Cgreen(%ad - %cr) %C(bold blue)<%an>%Creset %s' --abbrev-commit --name-status"
alias glg="git log --oneline --graph --pretty=format:'%Cred%h%Creset - %C(yellow)%d %Cgreen(%cr) %C(bold blue)<%an>%Creset %<(50,trunc)%s'"
alias gsh="git show"
alias gm="git status | grep 'modifié' | sed 's/\(\t\)modifié//g' | sed 's/://g'"

function gb() {
    # Move to a branch if it contains the pattern (except if more than one branch match)
    matches="$(git branch | grep $1)"
    if [ $(echo "$matches" | wc -l) -gt 1 ]
    then
        echo "too much result to find the required branch..."
        echo "$matches"
        return 1
    else
        case $matches in
            *[*]* )
                echo "You are already on the found branch: $matches"
                return 1
                ;;
        esac
        git checkout $matches
    fi
}


alias h='history | awk '{ \$1=\"\"; print \$0 }' | grep -v -e source -e history -e clear -e exit -e ' h ' | sort | uniq'
